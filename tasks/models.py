from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

import abc


class AbstractTaskState(object):
    __meta__ = abc.ABCMeta


class TaskInit(AbstractTaskState):

    @staticmethod
    def avaiop():
        return ['devel']

    @staticmethod
    def identify():
        return 'init'

    @staticmethod
    def name():
        return 'Put back'

    @staticmethod
    def precheck(ins):
        pass

    @staticmethod
    def op(ins):
        return ins.dev

    @staticmethod
    def alter_op(ins):
        return None  # ins.dev

    @staticmethod
    def step():
        return 0


class TaskDevel(AbstractTaskState):

    @staticmethod
    def avaiop():
        return ['test', 'init']

    @staticmethod
    def identify():
        return 'devel'

    @staticmethod
    def name():
        return 'Devel'

    @staticmethod
    def precheck(ins):
        assert ins.dev

    @staticmethod
    def op(ins):
        return ins.dev

    @staticmethod
    def alter_op(ins):
        return ins.test

    @staticmethod
    def step():
        return 1


class TaskTest(AbstractTaskState):

    @staticmethod
    def avaiop():
        return ['finish', 'devel']

    @staticmethod
    def identify():
        return 'test'

    @staticmethod
    def name():
        return 'Test'

    @staticmethod
    def precheck(ins):
        assert ins.test

    @staticmethod
    def op(ins):
        return ins.test

    @staticmethod
    def alter_op(ins):
        return ins.dev

    @staticmethod
    def step():
        return 2


class TaskFinish(AbstractTaskState):

    @staticmethod
    def avaiop():
        return ['test']

    @staticmethod
    def identify():
        return 'finish'

    @staticmethod
    def name():
        return 'Finish'

    @staticmethod
    def precheck(ins):
        pass

    @staticmethod
    def op(ins):
        return ins.test

    @staticmethod
    def alter_op(ins):
        return ins.dev

    @staticmethod
    def step():
        return 3


class TaskDead(AbstractTaskState):

    @staticmethod
    def avaiop():
        return ['init']

    @staticmethod
    def identify():
        return 'dead'

    @staticmethod
    def name():
        return 'Dead'

    @staticmethod
    def precheck(ins):
        pass

    @staticmethod
    def op(ins):
        return None

    @staticmethod
    def step():
        return 0


class Tag(models.Model):
    name = models.CharField(max_length=32)

    def __unicode__(self):
        return self.name


class Value(models.Model):
    tag = models.ForeignKey('Tag')  # ,related_name='values')
    task = models.ForeignKey('Task')
    value = models.CharField(max_length=64, blank=True, default='')


class Task(models.Model):

    available_state = {
        'init': TaskInit,
        'devel': TaskDevel,
        'test': TaskTest,
        'finish': TaskFinish,
        'dead': TaskDead,
    }

    title = models.CharField(max_length=64, null=False, blank=False)
    create_date = models.DateTimeField(auto_now_add=True)
    state = models.CharField(max_length=16, blank=False, null=False, choices=(
        ('init', u'init'),
        ('devel', u'devel'),
        ('test', u'test'),
        ('finish', u'finish'),
        ('dead', u'dead'),), default='init')
    dev = models.ForeignKey(User, null=True, default=None,
                            related_name='my_dev_tasks')
    test = models.ForeignKey(
        User, null=True, default=None,
        related_name='my_test_tasks')
    desc = models.TextField(blank=True, null=False, default='')

    def __unicode__(self):
        return u'[%s] %s' % (self.state, self.title)

    @property
    def state_obj(self):
        return self.available_state[self.state]

    @property
    def available_op(self):
        return [self.available_state[sn] for sn in self.available_state[self.state].avaiop()]

    @property
    def op(self):
        return self.available_state[self.state].op(self)

    @property
    def alter_op(self):
        return self.available_state[self.state].alter_op(self)

    def convert(self, state_name):
        clz = self.available_state[state_name]
        clz.precheck(self)

        self.state = clz.identify()
        self.save()
