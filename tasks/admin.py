from django.contrib import admin

# Register your models here.
from models import Tag, Task, Value


class ValueInline(admin.TabularInline):
    model = Value


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    inlines = [ValueInline]
