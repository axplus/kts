from django.conf.urls import url
from . import views


app_name = 'tasks'
urlpatterns = [
    url(r'^$', views.list, name='list'),
    url(r'^tags/(?P<tag>[^ ]+)$', views.listwithtag, name='listwithtag'),

    # url(r'^add$', views.add, name='add'),
    url(r'^fastadd$', views.fastadd, name='fastadd'),

    # url(r'^(?P<task_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^(?P<task_id>\d+)/edit$', views.edit, name='edit'),
    url(r'^(?P<task_id>\d+)/delete$', views.delete, name='delete'),
    url(r'^(?P<task_id>\d+)/convertto/(?P<identify>\w+)$',
        views.convertto, name='convertto'),




    # util
    url(r'^csv/', views.csv, name='csv'),
    url(r'^importall$', views.importall, name='importall'),
]
