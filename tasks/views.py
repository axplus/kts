from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse

from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import Task, Tag


from django.forms import ModelForm
from cStringIO import StringIO
import csv as csvm


class TaskForm(ModelForm):

    class Meta:
        model = Task
        fields = ['title', 'dev', 'test', 'desc']


@login_required
def list(request):
    tags = Tag.objects.all()
    tasks = Task.objects.exclude(
        state='dead'
    ).exclude(
        state='finish'
    )
    tasks = sorted(tasks,
                   key=lambda task: u'%20s-%20s' % (10 - task.state_obj.step(), task.op))
    return render(request, 'list.html', dict(tags=tags, tasks=tasks))


@login_required
def listwithtag(request, tag):
    if tag == 'untagged':
        pass
    else:
        tag = get_object_or_404(Tag, name=tag)
        values = tag.value_set.all()
        tasks = []
        for value in values:
            tasks.append(value.task)
    tags = Tags.objects.annotate(Count('value_set'))
    return render(request, 'list.html', {'tasks': tasks, 'tags': tags})


@login_required
def detail(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    return render(request, 'detail.html', {'task': task})


@login_required
def edit(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            task.save()
    else:
        form = TaskForm(instance=task)
    return render(request, 'edit.html', {'task': task, 'form': form})


@login_required
def delete(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    task.convert('dead')

    return redirect(reverse('tasks:list'))


@login_required
def convertto(request, task_id, identify):
    task = get_object_or_404(Task, pk=task_id)
    task.convert(identify)  # 'dead')

    prev_url = request.META.get('HTTP_REFERER', reverse('tasks:list'))

    return redirect(prev_url)  # reverse('tasks:edit', args=[task.id]))


@login_required
def fastadd(request):
    if request.method == 'POST':
        title = request.POST['title']
        task = Task(title=title)
        task.save()
        return redirect(reverse('tasks:list'))


@login_required
def importall(request):
    if request.method == 'POST':
        txt = request.POST['titles']
        for title in txt.split('\n'):
            task = Task(title=title)
            task.save()
        return redirect(reverse('tasks:list'))
    else:
        return render(request, 'importall.html')


@login_required
def csv(request):
    sio = StringIO()
    try:
        writer = csvm.DictWriter(
            sio, fieldnames=['state', 'title', 'dev', 'test', 'createdate'])
        writer.writeheader()
        alltasks = Task.objects.exclude(state='dead').exclude(state='finish')
        for task in alltasks:
            linearr = {
                'state': task.state,
                'title': task.title,
                'dev': task.dev,
                'test': task.test,
                'createdate': task.create_date}
            strarr = {k: unicode(o).encode('utf-8')
                      for k, o in linearr.items()}
            writer.writerow(strarr)
        return HttpResponse(sio.getvalue(), content_type='application/csv')
    finally:
        sio.close()
